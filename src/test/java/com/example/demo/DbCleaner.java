package com.example.demo;

import com.example.demo.User.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DbCleaner {

  @Autowired
  private UserRepo userRepo;

  public void cleanAll() {
    userRepo.deleteAll();
  }


}
