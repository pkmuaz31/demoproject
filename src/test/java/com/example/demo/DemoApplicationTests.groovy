package com.example.demo

import com.example.demo.generated.server.model.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import static org.springframework.http.HttpMethod.GET
import static org.springframework.http.HttpMethod.POST
import static org.springframework.http.HttpStatus.CONFLICT
import static org.springframework.http.HttpStatus.OK

@SpringBootTest(webEnvironment = RANDOM_PORT)
@ActiveProfiles('test')
class DemoApplicationTests extends Specification {

  @Autowired
  protected TestRestTemplate restTemplate

  @Autowired
  private DbCleaner dbCleaner

  def cleanup() {
    dbCleaner.cleanAll()
  }

  ResponseEntity sendRequest(String path, HttpMethod method, Map allParams) {
    def passedData
    Object requestBody = allParams?.remove("requestBody")
    if (requestBody) {
      passedData = requestBody
    } else {
      passedData = new LinkedMultiValueMap<String, String>()
      allParams.each { k, v -> passedData.set(k, v) }
    }
    def requestHeaders = new HttpHeaders()
    requestHeaders.set("Content-Type", "application/json")
    def requestEntity = new HttpEntity<MultiValueMap<String, String>>(passedData, requestHeaders)
    restTemplate.exchange(path, method, requestEntity, new ParameterizedTypeReference<Object>() {})
  }

  def "when there is no user"() {

    when:
    ResponseEntity<List<User>> response = sendRequest('/user', GET, null)

    then:
    response.statusCode == OK
    response.body.size() == 0
  }

  def "add user with valid username"() {

    given:
    User user = new User(username: "test", email: "test", password: 'test')

    when:
    ResponseEntity<Boolean> response = sendRequest('/user', POST, [requestBody: user])

    then:
    response.statusCode == OK
    response.body
  }

  def "when same user added twice gives conflict error"() {

    given:
    User user = new User(username: "test", email: "test", password: 'test')

    when:
    ResponseEntity<Boolean> response = sendRequest('/user', POST, [requestBody: user])
    ResponseEntity<Boolean> response2 = sendRequest('/user', POST, [requestBody: user])

    then:
    response.statusCode == OK
    response.body
    response2.statusCode == CONFLICT
  }

  def "get user after saving one"() {
    given:
    User user = new User(username: "test", email: "test", password: 'test')
    sendRequest('/user', POST, [requestBody: user])


    when:
    ResponseEntity<List<User>> response = sendRequest('/user', GET, null)

    then:
    response.statusCode == OK
    response.body.size() == 1
  }

}
