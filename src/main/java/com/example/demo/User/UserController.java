package com.example.demo.User;

import com.example.demo.generated.server.api.UserApi;
import com.example.demo.generated.server.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
public class UserController implements UserApi {

  private UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @Override
  public ResponseEntity<List<User>> getAllUsers() {
    return ResponseEntity.ok(userService.getUserInfo());
  }

  @Override
  public ResponseEntity<Boolean> saveUserInfo(@Valid @RequestBody User saveUser) {
    Boolean result = userService.saveUserInfo(saveUser);
    return result == null ? ResponseEntity.status(HttpStatus.CONFLICT).build() : ResponseEntity.ok(result);
  }
}
