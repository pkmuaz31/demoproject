package com.example.demo.User;

import com.example.demo.generated.server.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

  private UserRepo userRepo;

  @Autowired
  public UserService(UserRepo userRepo) {
    this.userRepo = userRepo;
  }

  List<User> getUserInfo() {
    return userRepo.getUserInfo();
  }

  Boolean saveUserInfo(User user) {
    if (!userRepo.isUserOrEmailAlreadyExits(user.getUsername(), user.getEmail()))
      return userRepo.saveUserInfo(user);
    return null;
  }
}
