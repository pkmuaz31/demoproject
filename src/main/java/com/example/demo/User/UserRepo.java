package com.example.demo.User;

import com.example.demo.generated.server.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepo {

  private JdbcTemplate jdbcTemplate;

  @Autowired
  public UserRepo(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  List<User> getUserInfo() {
    String query = "SELECT USERID, \"NAME\", USERNAME, PASSWORD, EMAIL, ISACTIVE FROM PUBLIC.\"USER\";";
    return jdbcTemplate.query(query, new BeanPropertyRowMapper<>(User.class));
  }

  Boolean saveUserInfo(User user) {
    String insertQuery = "INSERT INTO PUBLIC.\"USER\"" +
        "(\"NAME\", USERNAME, PASSWORD, EMAIL, ISACTIVE)" +
        "VALUES(?, ?, ?, ?, ?);";
    return jdbcTemplate.update(insertQuery, user.getFullname(), user.getUsername(), user.getPassword(), user.getEmail(), true) > 0;
  }

  public void deleteAll() {
    String query = "TRUNCATE TABLE PUBLIC.\"USER\"";
    jdbcTemplate.update(query);
  }

  boolean isUserOrEmailAlreadyExits(String userName, String email) {
    String query = "SELECT COUNT(1) FROM PUBLIC.\"USER\" WHERE USERNAME = ? OR EMAIL = ?";
    return jdbcTemplate.queryForObject(query, Integer.class, userName, email) > 0;
  }
}
